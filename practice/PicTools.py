#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  PicTools.py
#  
#  Copyright 2015 Unknown <root@hp425>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

from gi.repository import Gtk, Pango
import pdb, getpass, os.path


class Editor(Gtk.ScrolledWindow):
    def __init__(self):
        super(Editor, self).__init__()
        self.editor = Gtk.TextView()
        self.buffer = Gtk.TextBuffer()
        self.buffer.connect("changed", self.buffer_changed)
        self.editor.set_buffer(self.buffer)
        self.active_file = None
        self.modified = False

        fdesc = Pango.FontDescription("Monospace 10")
        self.editor.modify_font(fdesc)
        
        self.add(self.editor)
        
    def buffer_changed(self, bff):
        self.modified = True
        
    def set_text(self, text):
        self.buffer.set_text(text)
        
    def get_text(self):
        start_iter = self.buffer.get_start_iter()
        end_iter = self.buffer.get_end_iter()
        text = self.buffer.get_text(start_iter, end_iter, False)
        return text

    def confirm_lose_changes(self):
        if not self.modified:
            return True
        md = Gtk.MessageDialog(flags = Gtk.DialogFlags.DESTROY_WITH_PARENT,
                               message_type = Gtk.MessageType.WARNING,
                               message_format = "The current file was modified. Lose changes?",
                               buttons = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                          "Don't save", 1,
                                          "Save", Gtk.ResponseType.OK))
        ret = md.run()
        if ret == Gtk.ResponseType.OK:
            self.save_file_activated(None)
        md.destroy()
        return ret != Gtk.ResponseType.CANCEL

    def new_file_activated(self, *args):
        # Controlar si existe un archivo anterior que fue modificado
        if not self.confirm_lose_changes():
            return
        # Borrar contenido editor
        self.buffer.set_text("")
        # Marcar el documento como NO modificado
        self.modified = False
        
    def open_file_activated(self, *args):
        # Controlar si existe un archivo anterior que fue modificado
        if not self.confirm_lose_changes():
            return
        # Seleccionar al archivo a cargar
        fc = Gtk.FileChooserDialog(title = "Open assembler file",
                                   action = Gtk.FileChooserAction.OPEN,
                                   buttons = (Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT,
                                              Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL))
        if fc.run() == Gtk.ResponseType.ACCEPT:
            self.active_file = fc.get_filename()
            with open(self.active_file, 'r') as inf:
                self.buffer.set_text(inf.read())
            self.modified = False
        fc.destroy()
               
    def save_file_activated(self, *args):
        if self.active_file == None:
            saved = self.save_as_file_activated()
        else:
            saved = self.save_active_file()
        return saved
            
    def save_as_file_activated(self, *args):
        # Seleccionar al archivo a cargar
        fc = Gtk.FileChooserDialog(title = "Save assembler file",
                                   action = Gtk.FileChooserAction.SAVE,
                                   buttons = (Gtk.STOCK_SAVE, Gtk.ResponseType.ACCEPT,
                                              Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL))
        if self.active_file is None:
            fc.set_current_folder("/home/" + getpass.getuser())
            fc.set_current_name("untitled.asm")
        else:
            fc.set_filename(self.active_file)

        saved = False
        canceled = False
        validated = False
        while not canceled and not validated:
            fcAnswer = fc.run()
            #fcAnswer = fcAnswer if fcAnswer != -4 else Gtk.ResponseType.CANCEL
            canceled = fcAnswer != Gtk.ResponseType.ACCEPT
            if not canceled:
                if self.active_file == fc.get_filename() or self.accept_overwrite(fc):
                    validated = True
                    self.active_file = fc.get_filename()
                    saved = self.save_active_file()
                    self.modified = False
        fc.destroy()
        return saved

    def save_active_file(self):
        with open(self.active_file, 'w') as outf:
            text = self.buffer.get_text(self.buffer.get_start_iter(),
                                        self.buffer.get_end_iter(), False)
            outf.write(text)
            return True
        return False

    def accept_overwrite(self, dialog):
        selected_file = dialog.get_filename()
        if self.active_file == selected_file or not os.path.isfile(selected_file):
            return True
        file_name = selected_file.split('/')
        file_name = file_name[len(file_name)-1]
        md = Gtk.MessageDialog(parent = dialog,
                               flags = Gtk.DialogFlags.DESTROY_WITH_PARENT,
                               message_type = Gtk.MessageType.WARNING,
                               message_format = "A file named '" + file_name + "' already exists." +
                                                "Do you want to replace it?",
                               buttons = (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                          "Replace", Gtk.ResponseType.ACCEPT))
        replace = md.run() == Gtk.ResponseType.ACCEPT
        md.destroy()
        return replace


class MainMenu(Gtk.MenuBar):
    def __init__(self, window, editor):
        super(MainMenu, self).__init__()
        self.window = window
        self.editor = editor
        
        # Menu FILE:
        file_menu = Gtk.Menu()

        for ref, met in (("New File", editor.new_file_activated),
                         ("Open", editor.open_file_activated),
                         ("Save", editor.save_file_activated),
                         ("Save As...", editor.save_as_file_activated),
                         (None, None),
                         ("Quit", window.quit_activated)):
            if ref == None:
                item = Gtk.SeparatorMenuItem()
            else:
                item = Gtk.ImageMenuItem(ref)
                item.connect("activate", met)
            file_menu.append(item)
        
        item = Gtk.MenuItem("File")
        item.set_submenu(file_menu)
        
        self.append(item)


class MainWindow(Gtk.Window):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.set_size_request(500, 400)
        self.connect('delete_event', self.quit_activated)

        # Make the editor
        self.editor = Editor()
        # Make the main menu
        self.menu = MainMenu(self, self.editor)
        # Make the toolbar
        
        vBox = Gtk.VBox()
        vBox.pack_start(self.menu, False, True, 0)
        vBox.pack_start(self.editor, True, True, 0)
        
        self.add(vBox)
        self.show_all()

    def quit_activated(self, *args):
        res = self.editor.confirm_lose_changes()
        if res:
            Gtk.main_quit()
        return not res

    def run(self):
        Gtk.main()


def main():
    mainWindow = MainWindow()
    mainWindow.run()
    return 0

if __name__ == '__main__':
    main()
